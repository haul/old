<?php namespace App\Favorites;

class FavoriteRepository {

    public function findByUserHaul($user_id, $haul_id)
    {
        return Favorite::where('user_id', $user_id)->where('haul_id', $haul_id)->first();
    }

}