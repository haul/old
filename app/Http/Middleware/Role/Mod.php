<?php namespace App\Http\Middleware\Role;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Mod {

	/**
	 * The guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @param  ResponseFactory  $response
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->auth->user()->role->isMod())
		{
			return $next($request);
		}

		return redirect()->home();
	}

}