<?php namespace App\Http\Middleware\Haul;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Hauls\HaulRepository;

class HaulModify {

	protected $haul;

	protected $auth;

	public function __construct(HaulRepository $haul, Guard $auth)
	{
		$this->haul = $haul;

		$this->auth = $auth;
	}

	public function handle($request, Closure $next)
    {
        if ( ! $this->isModOrOwner($request))
        {
            return new RedirectResponse(route('home'));
        }

        return $next($request);
    }

    private function isModOrOwner($request)
    {
    	$user = $this->auth->user();
    	$haul_id = $request->route()->getParameter('id');

    	if ($user->role->isMod())
    	{
    		return true;
    	}

    	if ($this->haul->isOwner($haul_id, $user->user_id))
    	{
    		return true;
    	}

    	return false;
    }

}