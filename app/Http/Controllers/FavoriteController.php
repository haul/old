<?php namespace App\Http\Controllers;

use App\Favorites\Favorite;
use App\Favorites\FavoriteRepository;
use App\Hauls\HaulRepository;
use App\Http\Requests\Favorite\FavoriteRequest;
use App\Http\Requests\Favorite\DeleteFavoriteRequest;
use Illuminate\Contracts\Auth\Guard;
use Laracasts\Flash\Flash;

class FavoriteController extends Controller
{
	protected $haulRepo;

	protected $favRepo;

	protected $auth;

	public function __construct(HaulRepository $haulRepo, FavoriteRepository $favRepo, Guard $auth)
	{
		$this->middleware('auth');

		$this->haulRepo = $haulRepo;

		$this->favRepo = $favRepo;

		$this->auth = $auth;

		parent::__construct();
	}

	public function store(FavoriteRequest $request)
	{
		Favorite::create([
			'haul_id' => $request->get('haul'),
			'user_id' => $this->auth->user()->id
		]);

		Flash::message('Favorite\'d');

		return redirect()->back();
	}

	public function destroy(DeleteFavoriteRequest $request)
	{
		$favorite = $this->favRepo->findByUserHaul($this->auth->user()->id, $request->get('haul'));

		$favorite->delete();

		Flash::message('Un-favorite\'d');

		return redirect()->back();
	}
}