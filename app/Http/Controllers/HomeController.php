<?php namespace App\Http\Controllers;

use App\Hauls\HaulRepository;
use App\Users\UserRepository;
use App\Bumps\BumpRepository;
use App\Comments\CommentRepository;

class HomeController extends Controller {

	protected $haulRepo;

	protected $commentRepo;

	protected $bumpRepo;

	public function __construct(HaulRepository $haulRepo, CommentRepository $commentRepo, BumpRepository $bumpRepo)
	{
		$this->haulRepo = $haulRepo;

		$this->commentRepo = $commentRepo;

		$this->bumpRepo = $bumpRepo;

		parent::__construct();
	}

	public function index()
	{
		$latestHauls = $this->haulRepo->getLatestPaginated(18);
		$latestHauls->setPath('haul');

		$latestBumps = $this->bumpRepo->getLatestPaginated(18);

		$latestComments = $this->commentRepo->findByLatest(14);
		$latestComments->setPath('comment');

		return view('homepage.index', compact('latestHauls', 'latestComments', 'latestBumps'));
	}

}
