<?php namespace App\Http\Controllers;

use App\Comments\CommentRepository;
use App\Hauls\HaulRepository;
use App\Users\UserRepository;

class ReportsController extends Controller {

	protected $haulRepo;

	protected $comRepo;

	protected $userRepo;

	public function __construct(HaulRepository $haulRepo, CommentRepository $comRepo, UserRepository $userRepo)
	{
		$this->middleware('auth');
		$this->middleware('mod');

		$this->haulRepo = $haulRepo;

		$this->comRepo = $comRepo;

		$this->userRepo = $userRepo;

		parent::__construct();
	}

	public function overview()
	{
		$haulData = [
			'weekly' => '',
			'monthly' => '',
			'yearly' => '',
		];

		return view('admin.reports.overview');
	}

	public function haulStats()
	{

	}

	public function commentStats()
	{

	}

	public function userStats()
	{

	}

	public function favoriteStats()
	{

	}

	public function viewStats()
	{

	}

	public function searchStats()
	{

	}

}