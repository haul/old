<?php namespace App\Http\Controllers;

use App\Http\Requests\Profile\ChangePasswordRequest;
use App\Http\Requests\Profile\EditProfileRequest;
use App\Http\Requests\Profile\UpdateProfileRequest;
use App\Users\UserRepository;
use Laracasts\Flash\Flash;

class ProfileController extends Controller {

    protected $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->middleware('auth', ['only' => ['edit', 'update', 'avatar']]);

        $this->userRepo = $userRepo;

        parent::__construct();
    }

    public function index()
    {
        $members = $this->userRepo->getLatestPaginated(50);

        return view('profile.list', compact('members'));
    }

    public function show($username)
    {
        $user = $this->userRepo->findByUsername($username);

        return view('profile.show', compact('user'));
    }

    public function avatar($username, EditProfileRequest $request)
    {
        $user = $this->userRepo->findByUsername($username);

        return view('profile.avatar.edit', compact('user'));
    }

    public function edit($username, EditProfileRequest $request)
    {
        $user = $this->userRepo->findByUsername($username);

        return view('profile.edit', compact('user'));
    }

    public function update($username, UpdateProfileRequest $request)
    {
        $user = $this->userRepo->findByUsername($username);

        $user->update([
            'title' => $request->get('title')
        ]);

        $user->save();

        Flash::message('Profile updated!');

        return redirect()->back();//route('profile.show', [$username]);
    }

    public function destroy($username)
    {
        $user = $this->userRepo->findByUsername($username);

        $user->delete();

        Flash::message('User deleted!');

        return redirect()->back();//route('profile.edit', [$username]);
    }

    public function restore($username)
    {
        $user = $this->userRepo->findByUsername($username);

        $user->restore();

        Flash::message('User restored!');

        return redirect()->back();//route('profile.edit', [$username]);
    }

}