<?php namespace App\Http\Controllers;

class PageController extends Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function tos()
	{
		return view('tos.index');
	}

	public function privacy()
	{
		return view('privacy.index');
	}

}