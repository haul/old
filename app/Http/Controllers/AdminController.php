<?php namespace App\Http\Controllers;

use App\Comments\CommentRepository;
use App\Hauls\HaulRepository;
use App\Users\UserRepository;

class AdminController extends Controller {

	protected $haulRepo;

	protected $comRepo;

	protected $userRepo;

	public function __construct(HaulRepository $haulRepo, CommentRepository $comRepo, UserRepository $userRepo)
	{
		$this->middleware('auth');
		$this->middleware('mod');

		$this->haulRepo = $haulRepo;

		$this->comRepo = $comRepo;

		$this->userRepo = $userRepo;

		parent::__construct();
	}

	public function settings()
	{
		return view('admin.settings');
	}

	public function deletedHauls()
	{
		$hauls = $this->haulRepo->getDeletedPaginated();

		return view('admin.deletedHauls', compact('hauls'));
	}

	public function deletedComments()
	{
		$comments = $this->comRepo->getDeletedPaginated();

		return view('admin.deletedComments', compact('comments'));
	}

	public function deletedUsers()
	{
		$users = $this->userRepo->getDeletedPaginated();

		return view('admin.deletedUsers', compact('users'));
	}

}