<?php namespace App\Http\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Config;
use Laracasts\Flash\Flash;

use App\Hauls\HaulRepository;
use App\Http\Requests\Haul\UpdateHaulRequest;
use App\Users\UserRepository;

class HaulController extends Controller {

    protected $haulRepo;

    protected $userRepo;

    protected $auth;

    public function __construct(HaulRepository $haulRepo, UserRepository $userRepo, Guard $auth)
    {
        $this->middleware('auth', ['only' => ['edit', 'update', 'destroy', 'restore']]);

        $this->middleware('can.modify.haul', ['only' => ['edit', 'update', 'destroy', 'restore']]);

        $this->haulRepo = $haulRepo;

        $this->userRepo = $userRepo;

        $this->auth = $auth;

        parent::__construct();
    }

    public function index()
    {
        $hauls = $this->haulRepo->getLatestPaginated(150);

        return view('haul.list', compact('hauls'));
    }

    public function show($id)
    {
        $haul = $this->haulRepo->findById($id);

        if ($haul->trashed())
        {
            Flash::message('Haul was deleted.');

            return redirect()->route('home');
        }

        return view('haul.show', compact('haul'));
    }

    public function random(Store $session)
    {
        $previous = $session->get('random', [0]);

        $haul = $this->haulRepo->random($previous);

        $session->push('random', $haul->id);

        if (count($previous) >= Config::get('haul.randoms'))
        {
            $tmp = $session->get('random');

            array_shift($tmp);

            $session->put('random', $tmp);
        }

        return redirect()->route('haul.show', [$haul->id]);
    }

    public function edit($id)
    {
        $haul = $this->haulRepo->findById($id);
        $members = $this->userRepo->getAll();

        return view('haul.edit', compact('haul', 'members'));
    }

    public function update($id, UpdateHaulRequest $request)
    {
        $haul = $this->haulRepo->findById($id);

        $data = [];

        if ($user_id = $request->get('userid'))
        {
            $data['user_id'] = $user_id;
        }

        if ($caption = $request->get('caption'))
        {
            $data['caption'] = $caption;
        }

        if ( ! count($data))
        {
            Flash::message('There was a problem.');

            return redirect()->back();
        }

        $haul->update($data);

        $haul->save();

        Flash::message('Updated!');

        return redirect()->back();//route('haul.show', [$id]);
    }

    public function destroy($id)
    {
        $haul = $this->haulRepo->findByid($id);

        $haul->delete();

        Flash::message('Haul deleted!');

        return redirect()->back();//route('haul.edit', [$id]);
    }

    public function restore($id)
    {
        $haul = $this->haulRepo->findById($id);

        $haul->restore();

        Flash::message('Haul restored!');

        return redirect()->back();//route('haul.edit', [$id]);
    }

}
