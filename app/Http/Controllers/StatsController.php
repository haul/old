<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Laracasts\Flash\Flash;

use Cache;

use App\Hauls\Haul as Haul;
use App\Users\User as User;
use App\Comments\Comment as Comment;
use App\Favorites\Favorite as Favorite;

class StatsController extends Controller {

	public function __construct()
	{
    	Cache::rememberForever('stats.haulsinbetween', function() {
    		return $haulsinbetween = Haul::withTrashed()->where('id', '>', 14000)->get();
    	});

/*
    	Cache::rememberForever('stats.topuploaders', function() {
    		return Cache::get('stats.haulsinbetween')->where
    	});
*/
		parent::__construct();
	}

    public function index()
    {
    	$haulsinbetween = Cache::get('stats.haulsinbetween');

    	Cache::rememberForever('stats.filesizetotal', function() use ($haulsinbetween) {
    		$filesize = 0;

    		foreach ($haulsinbetween as $haul) {
    			$filesize += $haul->size;
    		}

    		return $filesize;
    	});

    	Cache::rememberForever('stats.totaldimensions', function() use ($haulsinbetween) {
    		$width = 0;
    		$height = 0;

    		foreach ($haulsinbetween as $haul) {
    			$width += $haul->width;
    			$height += $haul->height;
    		}

    		return ['width' => $width, 'height' => $height];
    	});

    	$users = User::whereHas('hauls', function($q) {
    		$q->withTrashed()->where('id', '>', 14000);
    	})->get();

    	$topuploaders = [];
    	foreach ($users as $user) {
    		$topuploaders[] = ['user' => $user, 'uploadcount' => $user->hauls()->where('id', '>', 14000)->count()];
    	}

    	usort($topuploaders, function($a, $b) {
		    return $b['uploadcount'] - $a['uploadcount'];
		});

    	$commenttotal = Comment::withTrashed()->where('haul_id', '>', 14000)->count();

    	$favoritetotal = Favorite::where('haul_id', '>', 14000)->count();

    	$webmtotalcount = Haul::withTrashed()->where('ext', 'webm')->count();

    	$totalregistered = User::withTrashed()->count();

    	$totaluploadfilesize = Haul::all()->sum('size');

		$data = [
			'sinceLastMilestone' => Haul::find(14000)->created_at->DiffForHumans(),
			'fifteenthousandthhaul' => Haul::find(15000),
			'filesizetotal' => $this->formatBytes(Cache::get('stats.filesizetotal')),
			'webmtotal' => $haulsinbetween->where('isImage', 0)->count(),
			'giftotal' => $haulsinbetween->where('ext', 'gif')->count(),
			'pngtotal' => $haulsinbetween->where('ext', 'png')->count(),
			'jpgtotal' => $haulsinbetween->where('ext', 'jpg')->count(),
			'pixelwidth' => Cache::get('stats.totaldimensions')['width'],
			'pixelheight' => Cache::get('stats.totaldimensions')['height'],
			'dailyviewcount' => 320,
			'topuploaders' => $topuploaders,
			'commenttotal' => $commenttotal,
			'favoritetotal' => $favoritetotal,
			'webmtotalcount' => $webmtotalcount,
			'totalregistered' => $totalregistered,
			'totaluploadcount' => Haul::withTrashed()->count(),
			'totaluploadfilesize' => $this->formatBytes($totaluploadfilesize),
			'pagecreationtime' => 3,
			'imagehaulversion' => 6,
			'nextversion' => 'Soon&#8482;'
		];

        return view('stats.index', compact('data'));
    }

    private function formatBytes($bytes, $precision = 2) { 
	    $base = log($bytes, 1024);
	    $suffixes = array('b', 'Kb', 'Mb', 'Gb', 'Tb');   

	    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)]; 
	} 

}
