<?php namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Filesystem\Cloud;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Config;

use Intervention\Image\ImageManager;
use Laracasts\Flash\Flash;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Coordinate\TimeCode;

use App\Hauls\HaulRepository;
use App\Http\Requests\Upload\UploadAvatarRequest;
use App\Http\Requests\Upload\UploadHaulRequest;

class UploadController extends Controller {

	private $formName;

	protected $haulRepo;

	protected $cloud;

	protected $filesystem;

	protected $image;

	protected $auth;

	protected $ffmpeg;
	protected $ffprobe;

	protected $isImage;

	public function __construct(HaulRepository $haulRepo, Cloud $cloud, ImageManager $image, Guard $auth, Filesystem $filesystem)
	{
		$this->middleware('auth');
		$this->middleware('plus');

		$this->haulRepo = $haulRepo;

		$this->cloud = $cloud;

		$this->image = $image;

		$this->auth = $auth;

		$this->filesystem = $filesystem;

		$this->ffmpeg = FFMpeg::create([
			'ffmpeg.binaries' => '/usr/bin/ffmpeg',
			'ffprobe.binaries' => '/usr/bin/ffprobe',
		]);

		$this->ffprobe = FFProbe::create([
			'ffmpeg.binaries' => '/usr/bin/ffmpeg',
			'ffprobe.binaries' => '/usr/bin/ffprobe'
		]);

		parent::__construct();
	}

	public function form()
	{
		return view('upload.form');
	}

	public function avatar(UploadAvatarRequest $request)
	{
		$this->formName = 'avatar';

		$file = $request->file($this->formName);

		$tmp = $file->getRealPath();

		$user = $this->auth->user();

		$errors = 0;

		$meme = $file->getClientMimeType();

		$data = [
			'filename' => md5($user->username) .'.'. $this->getExtension($meme),
		];

		$sizes = Config::get('haul.avatarThumbSizes');

		$thumbPath = Config::get('haul.thumbPath');

		foreach ($sizes as $key => $value)
		{
			$tmpThumb = $thumbPath.$value.$data['filename'];

			$this->image->make($tmp)->fit($value)->save($tmpThumb);

			$this->cloud->put('theavatars/'. $key .'/'. $data['filename'], file_get_contents($tmpThumb), 'public');

//			$this->image->make($tmp)->fit($value)->save('/var/www/haul/data/avatars/'. $key .'/'. $data['filename']);

//			if ( ! file_exists('/var/www/haul/data/avatars/'. $key .'/'. $data['filename']))
			if ( ! $this->cloud->exists('theavatars/'. $key .'/'. $data['filename']))
			{
				$errors++;
			}

			unlink($tmpThumb);
		}

		$user->update([
			'avatar' => $data['filename']
		]);

		$user->save();

		unlink($tmp);
//		unlink($file);

		if ($errors == 0)
		{
			Flash::success('Your haul has been uploaded successfully! Press f5 if it does not apply.');

			return redirect()->route('profile.show', [$user->username]);
		}

		Flash::error('Sorry looks like we had a server error (or maybe you broke it), please try re-uploading.');

		return redirect()->route('upload.form');
	}

	public function haul(UploadHaulRequest $request)
	{
		$file = $request->file(Config::get('haul.formNames.hauls'));

		$tmp = $file->getRealPath();

		$hash = md5_file($tmp);

		if ($dupe = $this->haulRepo->findByHash($hash))
		{
			unlink($tmp);

			Flash::message('We already have that picture, we don\'t need another.');

			return redirect()->route('haul.show', [$dupe->id]);
		}

		try {
			$measurements = getimagesize($tmp);
		} catch (\Exception $e) {
			$measurements = false;
		}

		$this->isImage = $measurements ? true : false;

		$mime = $file->getClientMimeType();

		if ( ! $this->isImage)
		{
			$webmTmp = storage_path() . '/tmp/' . str_random(6) . '.jpg';

			$dimensions = $this->ffprobe->streams($tmp)->videos()->first()->getDimensions();

			$this->ffmpeg->open($tmp)->frame(TimeCode::fromSeconds(1))->save($webmTmp);

			if ( ! file_exists($webmTmp))
			{
				Flash::error('Sorry looks like we had a server error (or maybe you broke it), please try re-uploading.');

				unlink($tmp);

				return redirect()->route('upload.form');
			}

			$measurements = [
				$dimensions->getWidth(),
				$dimensions->getHeight()
			];
		}

		$data = [
			'user_id' => $this->auth->user()->id,
			'filename' => $this->getFilename($file, $request->getClientIp(), $mime),
			'hash' => $hash,
			'ip' => ip2long($request->getClientIp()),
			'caption' => $this->getCaption($file->getClientOriginalName(), $request->get('caption')),
			'size' => $file->getClientSize(),
			'ext' => $this->getExtension($mime),
			'width' => $measurements[0],
			'height' => $measurements[1],
			'isImage' => (int) $this->isImage
		];

		$sizes = Config::get('haul.haulThumbSizes');

		$thumbPath = Config::get('haul.thumbPath');

		foreach ($sizes as $key => $size)
		{
			$tmpThumb = $thumbPath.$size.$data['filename'] . ( ! $this->isImage ? '.jpg' : '');

			$this->image->make($this->isImage ? $tmp : $webmTmp)->fit($size)->save($tmpThumb);

			$this->cloud->put('thethumbs/'. $key .'/'. $data['filename'] . ( ! $this->isImage ? '.jpg' : ''), file_get_contents($tmpThumb), 'public');

			unlink($tmpThumb);
//			$this->image->make($this->isImage ? $tmp : $webmTmp)->fit($size)->save('/var/www/haul/data/thumbs/'. $key .'/'. $data['filename'] . ( ! $this->isImage ? '.jpg' : ''));
		}

		if ( ! $this->isImage)
		{
			unlink($webmTmp);
		}

		$this->cloud->put('thehauls/'. $data['filename'], file_get_contents($tmp), 'public');

		unlink($tmp);

//		$file->move('/var/www/haul/data/hauls/', $data['filename']);

//		unset($file);

//		if (file_exists('/var/www/haul/data/hauls/'. $data['filename']))
		if ($this->cloud->exists('thehauls/'. $data['filename']))
		{
			$haul = $this->haulRepo->upload($data);

			Flash::success('Your haul has been uploaded successfully!');

			return redirect()->route('haul.show', [$haul->id]);
		}

		Flash::error('Sorry looks like we had a server error (or maybe you broke it), please try re-uploading.');

		return redirect()->route('upload.form');
	}

	private function getExtension($mime)
	{
		switch($mime)
		{
			case 'image/png':
				return 'png';
				break;

			default:
			case 'image/jpeg':
			case 'image/jpg':
				return 'jpg';
				break;

			case 'image/gif':
				return 'gif';
				break;
			/*
			case 'image/webp':
				return 'webp';
				break;
			*/
			case 'video/webm':
				return 'webm';
				break;
		}
	}

	private function getFilename($file, $ip, $mime)
	{
		return sha1(time()
			. str_random(5)
			. ip2long($ip)
			. $file->getClientOriginalName())
			. '.' . $this->getExtension($mime);
	}

	private function getCaption($clientName, $caption = null)
	{
		return $caption ?: str_replace(['-', '_'], ' ', preg_replace('/\\.[^.\\s]{3,4}$/', '', $clientName));
	}

}
