<?php namespace App\Http\Controllers;

use App\Bumps\Bump;
use App\Bumps\BumpRepository;
use App\Hauls\HaulRepository;
use App\Http\Requests\Bump\BumpRequest;
use App\Http\Requests\Bump\DeleteBumpRequest;
use Illuminate\Contracts\Auth\Guard;
use Laracasts\Flash\Flash;

use Config;

class BumpController extends Controller
{
	protected $bumpRepo;

	protected $haulRepo;

	protected $auth;

	public function __construct(BumpRepository $bumpRepo, HaulRepository $haulRepo, Guard $auth)
	{
		$this->middleware('auth');

		$this->bumpRepo = $bumpRepo;

		$this->haulRepo = $haulRepo;

		$this->auth = $auth;

		parent::__construct();
	}

	public function index()
	{
		$bumps = $this->bumpRepo->getLatestPaginated(20);

		return view('bump.list', compact('bumps'));
	}

	public function store(BumpRequest $request)
	{
		$bumpcount = Config::get('haul.bumpcount');

		$bumps = $this->bumpRepo->getLatestPaginated($bumpcount);
		
		if (count($bumps) == $bumpcount)
		{
			$bumps->last()->delete();
		}

		Bump::create([
			'haul_id' => $request->get('haul'),
			'user_id' => $this->auth->user()->id
		]);

		Flash::message('Bump\'d');

		return redirect()->back();
	}

}
