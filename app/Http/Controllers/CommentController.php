<?php namespace App\Http\Controllers;

use App\Comments\Comment;
use App\Comments\CommentRepository;
use App\Users\UserRepository;
use App\Http\Requests\Comment\EditCommentRequest;
use App\Http\Requests\Comment\PostCommentRequest;
use App\Http\Requests\Comment\UpdateCommentRequest;
use Illuminate\Auth\Guard;
use Laracasts\Flash\Flash;

class CommentController extends Controller {

    protected $comRepo;

    protected $userRepo;

    protected $auth;

    public function __construct(CommentRepository $comRepo, UserRepository $userRepo, Guard $auth)
    {
        $this->middleware('auth', ['except' => ['index']]);
        $this->middleware('mod', ['except' => ['index', 'store']]);

        $this->comRepo = $comRepo;

        $this->userRepo = $userRepo;

        $this->auth = $auth;

        parent::__construct();
    }

    public function index()
    {
        $comments = $this->comRepo->findByLatest(50);

        return view('comment.list', compact('comments'));
    }

    public function store(PostCommentRequest $request)
    {
        Comment::create([
            'user_id' => $this->auth->user()->id,
            'haul_id' => (int)$request->get('haul'),
            'body' => $request->get('body')
        ]);

        Flash::message('Comment posted!');

        return redirect()->back();
    }

    public function edit($id, EditCommentRequest $request)
    {
        $comment = $this->comRepo->findById($id);
        $members = $this->userRepo->getAll();

        return view('comment.edit', compact('comment', 'members'));
    }

    public function update($id, UpdateCommentRequest $request)
    {
        $comment = $this->comRepo->findById($id);

        $data = [];

        if ($user_id = $request->get('userid'))
        {
            $data['user_id'] = $user_id;
        }

        if ($body = $request->get('body'))
        {
            $data['body'] = $body;
        }

        if ( ! count($data))
        {
            Flash::message('There was a problem.');

            return redirect()->back();
        }

        $comment->update($data);

        $comment->save();

        Flash::message('Updated!');

        return redirect()->back();//route('haul.show', [$comment->haul->id]);
    }

    public function destroy($id)
    {
        $comment = $this->comRepo->findByid($id);

        $comment->delete();

        Flash::message('Comment deleted!');

        return redirect()->back();//route('comment.edit', [$id]);
    }

    public function restore($id)
    {
        $comment = $this->comRepo->findById($id);

        $comment->restore();

        Flash::message('Comment restored!');

        return redirect()->back();//route('comment.edit', [$id]);
    }

}