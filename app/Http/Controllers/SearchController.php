<?php namespace App\Http\Controllers;

use App\Hauls\HaulRepository;
use Input;

class SearchController extends Controller {

	protected $haulRepo;

	public function __construct(HaulRepository $haulRepo)
	{
		$this->haulRepo = $haulRepo;

		parent::__construct();
	}

	public function search()
	{
		if (Input::has('for')) {
			$for = Input::get('for');

			$hauls = $this->haulRepo->search($for);

			return view('search.index', compact('hauls', 'for'));
		}

		return view('search.index');
	}

}