<?php namespace App\Http\Requests\Bump;

use App\Hauls\HaulRepository;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Request;

class BumpRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'haul' => 'required|numeric|exists:hauls,id'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(HaulRepository $haulRepo, Guard $auth)
	{
		return $auth->check() and ( ! $haulRepo->findById($this->get('haul'))->bump);
	}

}
