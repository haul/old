<?php namespace App\Http\Requests\Password;

use Illuminate\Auth\Guard;
use App\Http\Requests\Request;

class UpdatePasswordRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'currentPassword' => 'required',
			'password' => 'required|confirmed'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(Guard $auth)
	{
		return false;
	}

}
