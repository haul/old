<?php namespace App\Http\Requests\Favorite;

use App\Favorites\FavoriteRepository;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Request;

class DeleteFavoriteRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(FavoriteRepository $favRepo, Guard $auth)
	{
		return $auth->check() and $favRepo->findByUserHaul($auth->user()->id, $this->get('haul'));
	}

}