<?php namespace App\Http\Requests\Haul;

use App\Hauls\HaulRepository;
use Illuminate\Auth\Guard;
use App\Http\Requests\Request;

class DeleteHaulRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(HaulRepository $haulRepo, Guard $auth)
	{
		$user = $auth->user();
		$haul_id = $this->route->parameter('id');

		if ($user->role->isMod() or $haulRepo->isOwner($haul_id, $user->id))
		{
			if ( ! $this->haulRepo->isDeleted($haul_id))
			{
				return true;
			}
		}

		return false;
	}

}