<?php namespace App\Http\Requests\Haul;

use App\Hauls\HaulRepository;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UpdateHaulRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'caption' => 'between:3,255',
			'user_id' => 'exists:users,id'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(HaulRepository $haulRepo, Guard $auth)
	{
		$user = $auth->user();

		if ($user->role->isMod() or $haulRepo->isOwner($this->route->parameter('id'), $user->id))
		{
			return true;
		}

		return false;
	}

	public function forbiddenResponse()
	{
		return new RedirectResponse(route('auth.login'));
	}

}