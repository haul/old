<?php namespace App\Http\Requests\Haul;

use App\Hauls\HaulRepository;
use Illuminate\Auth\Guard;
use App\Http\Requests\Request;
use Illuminate\Http\RedirectResponse;

class EditHaulRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(HaulRepository $haulRepo, Guard $auth)
	{
		$user = $auth->user();

		if ($user->role->isMod() or $haulRepo->isOwner($this->route->parameter('id'), $user->id))
		{
			return true;
		}

		return false;
	}

	public function forbiddenResponse()
	{
		return new RedirectResponse(route('auth.login'));
	}

}
