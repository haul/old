<?php namespace App\Http\Requests\Upload;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Config;

class UploadHaulRequest extends Request {

	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;

		$this->dontFlash[] = Config::get('haul.formNames.hauls');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'haul' => 'required|mimes:png,gif,jpeg,webm',
			'caption' => 'between:3,255',
			'tos' => 'accepted'
		];

		if ($this->auth->user()->role->isTrusted())
		{
			array_pop($rules);
		}

		return $rules;
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if ($this->auth->user()->role->isPlus())
		{
			return true;
		}

		return false;
	}

}
