<?php namespace App\Http\Requests\Upload;

use Illuminate\Auth\Guard;
use App\Http\Requests\Request;

class UploadAvatarRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'avatar' => 'required|image:png,gif,jpeg,jpg'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(Guard $auth)
	{
		if ($auth->user()->role->isPlus())
		{
			return true;
		}

		return false;
	}

}