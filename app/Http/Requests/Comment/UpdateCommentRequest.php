<?php namespace App\Http\Requests\Comment;

use Illuminate\Auth\Guard;
use App\Http\Requests\Request;
use Illuminate\Http\RedirectResponse;

class UpdateCommentRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'body' => 'required|between:3,255'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(Guard $auth)
	{
		$user = $auth->user();

		if ($user->role->isMod())
		{
			return true;
		}

		return false;
	}

	public function forbiddenResponse()
	{
		return new RedirectResponse(route('auth.login'));
	}

}