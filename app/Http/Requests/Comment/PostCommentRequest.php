<?php namespace App\Http\Requests\Comment;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Request;

class PostCommentRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'body' => 'required|between:3,255',
			'haul' => 'required|exists:hauls,id'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(Guard $auth)
	{
		return $auth->check();
	}

}