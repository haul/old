<?php namespace App\Http\Requests\Profile;

use Illuminate\Auth\Guard;
use App\Http\Requests\Request;
use Illuminate\Http\RedirectResponse;

class EditPasswordRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(Guard $auth)
	{
		$user = $auth->user();

		if ($this->route->parameter('username') == $user->username)
		{
			return true;
		}

		return false;
	}

	public function forbiddenResponse()
	{
		return new RedirectResponse(route('auth.login'));
	}

}
