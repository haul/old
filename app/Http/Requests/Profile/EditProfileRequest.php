<?php namespace App\Http\Requests\Profile;

use Illuminate\Auth\Guard;
use App\Http\Requests\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Route;

class EditProfileRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(Guard $auth, Route $route)
	{
		$user = $auth->user();

		if ($user->role->isMod() or $route->parameter('username') == $user->username)
		{
			return true;
		}

		return false;
	}

	public function forbiddenResponse()
	{
		return new RedirectResponse(route('auth.login'));
	}

}
