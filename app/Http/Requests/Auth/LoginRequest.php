<?php namespace App\Http\Requests\Auth;

use Illuminate\Http\RedirectResponse;

use App\Http\Requests\Request;

use App\Users\User;

use Flash;

class LoginRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'username' => 'required',
			'password' => 'required',
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}