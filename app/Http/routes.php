<?php

$router->pattern('id', '[0-9]+');

$router->group(['prefix' => '/'], function ($router)
{
    $router->get('hauls', [
        'as' => 'haul.list',
        'uses' => 'HaulController@index'
    ]);

    $router->get('members', [
        'as' => 'profile.list',
        'uses' => 'ProfileController@index'
    ]);

    $router->get('random', [
        'as' => 'haul.random',
        'uses' => 'HaulController@random'
    ]);

    $router->get('search', [
        'as' => 'search',
        'uses' => 'SearchController@search'
    ]);

});

$router->group(['prefix' => 'pages'], function ($router)
{
	$router->get('15k', [
		'as' => 'pages.15k',
		'uses' => 'StatsController@index'
	]);
    $router->get('tos', [
        'as' => 'pages.tos',
        'uses' => 'PageController@tos'
    ]);

    $router->get('privacy', [
        'as' => 'pages.privacy',
        'uses' => 'PageController@privacy'
    ]);

});

$router->group(['prefix' => 'admin'], function ($router)
{
    $router->get('settings', [
        'as' => 'admin.settings',
        'uses' => 'AdminController@settings'
    ]);

    $router->get('secret', [
        'as' => 'admin.secret',
        function() {
            return 'what are you doing!';
        }
    ]);

    $router->get('hauls', [
        'as' => 'admin.deletedHauls',
        'uses' => 'AdminController@deletedHauls'
    ]);

    $router->get('comments', [
        'as' => 'admin.deletedComments',
        'uses' => 'AdminController@deletedComments'
    ]);

    $router->get('users', [
        'as' => 'admin.deletedUsers',
        'uses' => 'AdminController@deletedUsers'
    ]);

    $router->get('reports', [
        'as' => 'admin.reports.overview',
        'uses' => 'ReportsController@overview'
    ]);

});

$router->group(['prefix' => 'bump'], function ($router)
{
    $router->post('/', [
        'as' => 'bump',
        'uses' => 'BumpController@store'
    ]);

    $router->get('list', [
        'as' => 'bump.list',
        'uses' => 'BumpController@index'
    ]);
});

$router->group(['prefix' => 'favorite'], function ($router)
{
    $router->post('/', [
        'as' => 'favorite',
        'uses' => 'FavoriteController@store'
    ]);

    $router->post('list', [
            'as' => 'favorite.list',
            'uses' => 'FavoriteController@list'
    ]);

    $router->post('delete', [
        'as' => 'favorite.delete',
        'uses' => 'FavoriteController@destroy'
    ]);

});

$router->group(['prefix' => 'upload'], function ($router)
{
    $router->get('/', [
        'as' => 'upload.form',
        'uses' => 'UploadController@form'
    ]);

    $router->post('/', [
        'as' => 'upload.haul',
        'uses' => 'UploadController@haul'
    ]);

    $router->post('/avatar', [
        'as' => 'upload.avatar',
        'uses' => 'UploadController@avatar'
    ]);

});

$router->group(['prefix' => '/'], function ($router)
{
    $router->get('{id}', [
        'as' => 'haul.show',
        'uses' => 'HaulController@show'
    ]);

    $router->get('{id}/edit', [
        'as' => 'haul.edit',
        'uses' => 'HaulController@edit'
    ]);

    $router->post('{id}/edit', [
        'as' => 'haul.update',
        'uses' => 'HaulController@update'
    ]);

    // change to delete
    $router->get('{id}/delete', [
        'as' => 'haul.delete',
        'uses' => 'HaulController@destroy'
    ]);

    // change to patch
    $router->get('{id}/restore', [
        'as' => 'haul.restore',
        'uses' => 'HaulController@restore'
    ]);

});

$router->group(['prefix' => 'profile'], function ($router)
{
    $router->get('{username}', [
        'as' => 'profile.show',
        'uses' => 'ProfileController@show'
    ]);

    $router->get('{username}/edit', [
        'as' => 'profile.edit',
        'uses' => 'ProfileController@edit'
    ]);

    $router->post('{username}/edit', [
        'as' => 'profile.update',
        'uses' => 'ProfileController@update'
    ]);

    $router->get('{username}/avatar/edit', [
        'as' => 'profile.avatar.edit',
        'uses' => 'ProfileController@avatar'
    ]);

    // change to delete
    $router->get('{username}/delete', [
        'as' => 'profile.delete',
        'uses' => 'ProfileController@destroy'
    ]);

    // change to patch
    $router->get('{username}/restore', [
        'as' => 'profile.restore',
        'uses' => 'ProfileController@restore'
    ]);

});

$router->group(['prefix' => 'comment'], function ($router)
{
    $router->post('post', [
        'as' => 'comment.store',
        'uses' => 'CommentController@store'
    ]);

    $router->get('list', [
        'as' => 'comment.list',
        'uses' => 'CommentController@index'
    ]);

    $router->get('{id}/edit', [
        'as' => 'comment.edit',
        'uses' => 'CommentController@edit'
    ]);

    $router->post('{id}/edit', [
        'as' => 'comment.update',
        'uses' => 'CommentController@update'
    ]);

    // change to delete
    $router->get('{id}/delete', [
        'as' => 'comment.delete',
        'uses' => 'CommentController@destroy'
    ]);

    $router->get('{id}/restore', [
        'as' => 'comment.restore',
        'uses' => 'CommentController@restore'
    ]);

});

$router->group(['prefix' => 'auth'], function ($router)
{
    $router->get('login', [
        'as' => 'auth.login',
        'uses' => 'Auth\AuthController@getLogin'
    ]);

    $router->post('login', [
        'as' => 'auth.login',
        'uses' => 'Auth\AuthController@postLogin'
    ]);

    $router->get('register', [
        'as' => 'auth.register',
        'uses' => 'Auth\AuthController@getRegister'
    ]);

    $router->post('register', [
        'as' => 'auth.register',
        'uses' => 'Auth\AuthController@postRegister'
    ]);

    $router->get('logout', [
        'as' => 'auth.logout',
        'uses' => 'Auth\AuthController@getLogout'
    ]);

});

$router->group(['prefix' => 'password'], function ($router)
{
    $router->get('remind', [
        'as' => 'password.remind',
        'uses' => 'Auth\PasswordController@getEmail'
    ]);

    $router->post('remind', [
        'as' => 'password.remind',
        'uses' => 'Auth\PasswordController@postEmail'
    ]);

    $router->get('reset/{token}', [
        'as' => 'password.reset',
        'uses' => 'Auth\PasswordController@getReset'
    ]);

    $router->post('reset', [
        'as' => 'password.reset',
        'uses' => 'Auth\PasswordController@postReset'
    ]);

    $router->post('change', [
        'as' => 'password.change',
        'uses' => 'Auth\PasswordController@changePassword'
    ]);

});

$router->get('/home', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

$router->get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);
