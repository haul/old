<?php namespace App\Roles;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    protected $table = 'roles';

    public $timestamps = false;

    protected $fillable = ['user_id', 'level'];

    public static $permissions = [
        'admin' => 5,
        'mod' => 4,
        'trusted' => 3,
        'plus' => 2,
        'member' => 1,
        'inactive' => 0
    ];

    public function scopeUser($query)
    {
        $query->where('user_id', $this->attributes['user_id']);
    }

    public function isAdmin()
    {
        return ($this->user()->first()->level >= static::$permissions['admin']);
    }

    public function isMod()
    {
        return ($this->user()->first()->level >= static::$permissions['mod']);
    }

    public function isTrusted()
    {
        return ($this->user()->first()->level >= static::$permissions['trusted']);
    }

    public function isPlus()
    {
        return ($this->user()->first()->level >= static::$permissions['plus']);
    }

    public function isMember()
    {
        return ($this->user()->first()->level >= static::$permissions['member']);
    }

    public function isInactive()
    {
        return ($this->user()->first()->level >= static::$permissions['inactive']);
    }

    public function hasPermission($rank)
    {
        return ($this->user()->first()->level >= static::$permissions[$rank]);
    }

    public function users()
    {
        return $this->hasMany('users');
    }

}