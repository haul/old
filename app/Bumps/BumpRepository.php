<?php namespace App\Bumps;

class BumpRepository {

    public function getLatestPaginated($limit = 5)
    {
        return Bump::latest()->paginate($limit);
    }

}
