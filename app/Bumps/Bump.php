<?php namespace App\Bumps;

use Illuminate\Database\Eloquent\Model;

class Bump extends Model {

    protected $table = 'bumps';

    protected $fillable = ['haul_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\Users\User');
    }

    public function haul()
    {
        return $this->belongsTo('App\Hauls\Haul');
    }

}