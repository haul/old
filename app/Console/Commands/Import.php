<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;

use App\Hauls\Haul;
use App\Users\User;
use App\Comments\Comment;

class Import extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'haul.import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import old database into new.';

	protected $db;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->db = '';//DB::connection('mysqlold');

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->hauls();
	}

	private function hauls()
	{
		$hauls = $this->db->select('select * from hauls where deleted_at is null');

		foreach ($hauls as $haul)
		{
			if ($haul->user_id > 1) $haul->user_id--; 

			Haul::create([
				'user_id' => $haul->user_id,
				'filename' => $haul->filename,
				'hash' => $haul->hash,
				'ip' => $haul->ip,
				'caption' => $haul->caption,
				'size' => $haul->size,
				'ext' => $haul->ext,
				'width' => $haul->width,
				'height' => $haul->height,
				'created_at' => $haul->created_at,
				'updated_at' => $haul->updated_at
			]);

			$this->info($haul->id . ' added.');
		}
	}

	private function comments()
	{
		$comments = $this->db->select('select * from comments where deleted_at is null');

		foreach ($comments as $comment)
		{
			if ($comment->user_id > 1) $comment->user_id--;

			Comment::create([
				'user_id' => $comment->user_id,
				'haul_id' => $comment->haul_id,
				'body' => $comment->body,
				'created_at' => $comment->created_at,
				'updated_at' => $comment->updated_at
			]);

			$this->info($comment->id . ' added.');
		}
	}

	private function users()
	{
		$users = $this->db->select('select * from users');
		
		array_shift($users);

		foreach ($users as $user)
		{
			User::create([
				'username' => $user->username,
				'email' => $user->email,
				'password' => 'temp',
				'created_at' => $user->created_at,
				'updated_at' => $user->updated_at
			]);

			$this->info($user->username . ' added.');
		}
	}
}
