<?php namespace App\Console\Commands;

use Config;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Cloud;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Hauls\HaulRepository;

class DeleteDeleted extends Command {

	protected $cloud;

	protected $haulRepo;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'haul:deletedeleted';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Delete deleted images on haul.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Cloud $cloud, HaulRepository $haulRepo)
	{
		$this->cloud = $cloud;

		$this->haulRepo = $haulRepo;

		parent::__construct();
	}

	public function fire()
	{
		$hauls = $this->haulRepo->getDeleted();

		$sizes = Config::get('haul.haulThumbSizes');

		foreach ($hauls as $haul)
		{
			foreach ($sizes as $key => $size) {
				if ($this->cloud->exists('thethumbs/' . $key . '/' . $haul->filename))
				{
					$this->cloud->delete('thethumbs/' . $key . '/' . $haul->filename);

					$this->info('- Deleted ' . $haul->id . '.');
				} else {
					$this->info('* Skipping ' . $haul->id . '.');
				}
			}
		}

		$this->info('# Done.');
	}
}
