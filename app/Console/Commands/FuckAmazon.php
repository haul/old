<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Cloud;
use Illuminate\Contracts\Filesystem\Filesystem;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Hauls\HaulRepository;
use App\Users\UserRepository;

class FuckAmazon extends Command {

	protected $cloud;

	protected $filesystem;

	protected $haulRepo;

	protected $userRepo;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'haul:fuckamazon';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Downloads everything in the amazon bucket and onto a specific location on localhost.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Cloud $cloud, Filesystem $filesystem, HaulRepository $haulRepo, UserRepository $userRepo)
	{
		$this->cloud = $cloud;

		$this->filesystem = $filesystem;

		$this->haulRepo = $haulRepo;

		$this->userRepo = $userRepo;

		parent::__construct();
	}

	public function fire()
	{
		$users = $this->userRepo->getAll();

		foreach ($users as $user)
		{
			$avatar = 'default.jpg';

			$file = $this->cloud->get('avatars/large/' . $avatar);
			$file2 = $this->cloud->get('avatars/medium/' . $avatar);
			$file3 = $this->cloud->get('avatars/small/' . $avatar);

			file_put_contents('/var/www/haul/data/avatars/large/' . $avatar, $file);
			file_put_contents('/var/www/haul/data/avatars/medium/' . $avatar, $file2);
			file_put_contents('/var/www/haul/data/avatars/small/' . $avatar, $file3);

			$this->info('Copied ' . $user->id . '.');
		}
	}

	public function haul()
	{
		$hauls = $this->haulRepo->getAll();

		foreach ($hauls->reverse() as $haul)
		{
			$filename = $haul->filename;

			$file = $this->cloud->get('hauls/' . $filename);
			file_put_contents('/var/www/haul/data/hauls/' . $filename, $file);

			if ( ! $haul->isImage)
			{
				$filename .= '.jpg';
			}

			$thumb1 = $this->cloud->get('thumbs/hauls/large/' . $filename);
			$thumb2 = $this->cloud->get('thumbs/hauls/medium/' . $filename);
			$thumb3 = $this->cloud->get('thumbs/hauls/small/' . $filename);

			file_put_contents('/var/www/haul/data/thumbs/large/' . $filename, $thumb1);
			file_put_contents('/var/www/haul/data/thumbs/medium/' . $filename, $thumb2);
			file_put_contents('/var/www/haul/data/thumbs/small/' . $filename, $thumb3);

			$this->info('+ Copied ' . $haul->id . '.');
		}

		$this->info('# Done.');

	}

}
