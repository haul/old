<?php namespace App\Console\Commands;

use App\Hauls\Haul;
use App\Hauls\HaulRepository;
use App\Users\UserRepository;

use Illuminate\Contracts\Filesystem\Cloud;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class amazon_migration extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'haul:amazonmigration';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Migrate all images to amazon s3.';

	protected $cloud;

	protected $haulRepo;

	protected $userRepo;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Cloud $cloud, HaulRepository $haulRepo, UserRepository $userRepo)
	{
		$this->cloud = $cloud;

		$this->haulRepo = $haulRepo;

		$this->userRepo = $userRepo;

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('# Uploading to amazon...');

		$this->avatars();

		//$this->hauls();

		$this->info('# done.');
	}

	private function avatars()
	{
		$users = $this->userRepo->getAll();

		foreach ($users as $user)
		{
			if ( ! $user->avatar) continue;

			foreach (['large', 'medium', 'small'] as $size)
			{
				$this->cloud->put('theavatars/' . $size . '/' . $user->avatar, file_get_contents('/var/www/haul/data/avatars/' . $size . '/' . $user->avatar));
			}

			$this->info('+ ' . $user->username . ' avatar added.');
		}
	}

	private function hauls()
	{
		$hauls = $this->haulRepo->getAll();
//		$hauls = Haul::where('id', '<', 5)->get();

		foreach ($hauls as $haul)
		{
			if ($this->cloud->exists('thehauls/' . $haul->filename)) continue;

			$this->cloud->put('thehauls/' . $haul->filename, file_get_contents('/var/www/haul/data/hauls/' . $haul->filename));

			foreach (['large', 'medium', 'small'] as $size)
			{
				$filename = ($haul->isImage) ? $haul->filename : $haul->filename . '.jpg';

				if ($this->cloud->exists('thethumbs/' . $size . '/' . $filename)) continue;

				$this->cloud->put('thethumbs/' . $size . '/' . $filename, file_get_contents('/var/www/haul/data/thumbs/' . $size . '/' . $filename));
			}

			$this->info('+ [' . $haul->id . '] ' . $haul->caption . ' added.');
		}
	}

}
