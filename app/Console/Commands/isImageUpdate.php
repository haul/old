<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Hauls\Haul;

class isImageUpdate extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'haul.isimage';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fix isImage columns in database.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$hauls = Haul::all();

		foreach ($hauls as $haul)
		{
			$haul->isImage = 1;
			$haul->save();
			$this->info($haul->id . ' updated.');
		}
	}

}
