<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Users\User;

use Mail;

class PasswordResetEmailer extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'haul.passwords';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send emails to users with password reset links.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$users = User::all();

		foreach ($users as $user)
		{
			Mail::send('emails.rejoin', ['user' => $user], function($message) use ($user)
			{
			    $message->to($user->email, $user->username)->subject('Rejoin Imagehaul!');
			});
		}
	}

}
