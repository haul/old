<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Filesystem\Cloud;
use Illuminate\Contracts\Filesystem\Filesystem;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Intervention\Image\ImageManager;

use App\Hauls\Haul;

class Thumbnail extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'haul.thumbs';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate new thumbnails for all old hauls';

	protected $cloud;

	protected $image;

	protected $filesystem;

	public function __construct(Cloud $cloud, Filesystem $filesystem, ImageManager $image)
	{
		$this->cloud = $cloud;

		$this->image = $image;

		$this->filesystem = $filesystem;

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$hauls = Haul::where('id', '>', 11715)->get();

		foreach ($hauls as $haul)
		{
			$image = $this->cloud->get('hauls/' . $haul->filename);

			$sizes = Config::get('haul.haulThumbSizes');

			foreach ($sizes as $key => $value)
			{
				$tmp = storage_path() . '/tmp/' . $key . '/' . $haul->filename;

				$this->image->make($image)->fit($value)->save($tmp);

				$this->cloud->put('thumbs/hauls/'. $key .'/'. $haul->filename, file_get_contents($tmp), 'public');

				unlink($tmp);
				
				$this->info($haul->id . ' converted.');
			}
		}
	}

}
