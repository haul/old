<?php namespace App\Console\Commands;

use Config;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Cloud;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Hauls\HaulRepository;

class CheckMissingHauls extends Command {

	protected $cloud;

	protected $haulRepo;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'haul:checkmissinghauls';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check for any missing hauls on cloud.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Cloud $cloud, HaulRepository $haulRepo)
	{
		$this->cloud = $cloud;

		$this->haulRepo = $haulRepo;

		parent::__construct();
	}

	public function fire()
	{
		$hauls = $this->haulRepo->getAll();

		$sizes = Config::get('haul.haulThumbSizes');

		foreach ($hauls as $haul)
		{
			if ( ! $this->cloud->exists('thehauls/' . $haul->filename))
			{
				$this->info('! ['. $haul->id .'] ' . $haul->filename . ' does not exist.');
			}
		}

		$this->info('# Done.');
	}
}
