<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\amazon_migration',
		'App\Console\Commands\Import',
		'App\Console\Commands\Thumbnail',
		'App\Console\Commands\isImageUpdate',
		'App\Console\Commands\FuckAmazon',
		'App\Console\Commands\PasswordResetEmailer',
		'App\Console\Commands\DeleteDeleted',
		'App\Console\Commands\CheckMissingHauls',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();
	}

}
