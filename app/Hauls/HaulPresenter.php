<?php namespace App\Hauls;

use Laracasts\Presenter\Presenter;

use Config;

class HaulPresenter extends Presenter {

	public function imageUrl($filename, $thumb = null)
	{
		return Config::get('app.url') . '/' . (($thumb) ? 'thethumbs/' . $thumb : 'thehauls') . '/' . $filename;
	}

    public function amazonImageUrl($filename, $thumb = null)
    {
        return 'https://s3-' . getenv('AWS_REGION') . '.amazonaws.com/'. getenv('AWS_BUCKET') .'/'. (($thumb) ? 'thethumbs/'. $thumb .'/' : 'thehauls/') . $filename;
    }

}
