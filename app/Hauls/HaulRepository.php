<?php namespace App\Hauls;

class HaulRepository {

    public function count()
    {
        return Haul::count();
    }

    public function random($previous = [0])
    {
        return Haul::whereNotIn('id', $previous)->orderByRaw("RAND()")->first();
    }

    public function upload($data)
    {
        return Haul::create($data);
    }

    public function search($for)
    {
/*        $words = explode(' ', $for);

        $hauls = Haul::query();

        foreach ($words as $word)
        {
            $hauls->where('caption', 'like','%'. $word .'%');
        }
*/
	return Haul::query()->where('caption', 'like', '%' . $for . '%')->latest()->where('deleted_at', null)->get();
    }

    public function getLatestPaginated($limit = 5)
    {
        return Haul::latest()->paginate($limit);
    }

    public function getDeletedPaginated($limit = 5)
    {
        return Haul::onlyTrashed()->latest()->paginate($limit);
    }

    public function getDeleted()
    {
	return Haul::onlyTrashed()->get();
    }

    public function isOwner($id, $user_id)
    {
        return Haul::withTrashed()->where('id', $id)->whereHas('user', function ($q) use ($user_id)
        {
            $q->where('id', $user_id);
        })->count();
    }

    public function isDeleted($haul_id)
    {
        return Haul::onlyTrashed()->where('id', $haul_id)->firstOrFail();
    }

    public function getAll()
    {
        return Haul::all();
    }

    public function findById($id)
    {
        return Haul::withTrashed()->where('id', $id)->with('user', 'favorites', 'comments', 'views')->firstOrFail();
    }

    public function findByHash($hash)
    {
        return Haul::withTrashed()->where('hash', $hash)->first();
    }

    public function findByLatestFavorited($limit = 5)
    {
        return Haul::latest()->with('favorites')->has('favorites')->paginate($limit);
    }

    public function getBackupList($datetime)
    {
        return Haul::latest()->where('created_at', '>', $datetime)->get();
    }

}
