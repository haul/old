<?php namespace App\Hauls;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class Haul extends Model {

    use SoftDeletes, PresentableTrait;

    protected $table = 'hauls';

    protected $hidden = ['ip'];

    protected $guarded = ['id', 'deleted_at', 'created_at', 'updated_at'];

    protected $dates = ['deleted_at'];

    protected $presenter = 'App\Hauls\HaulPresenter';

    public function setIpAttribute($ip)
    {
        $this->attributes['ip'] = ip2long($ip);
    }

    public function getPrevious()
    {
        return self::where('id', '<', $this->id)->max('id');
    }

    public function getNext()
    {
        return self::where('id', '>', $this->id)->min('id');
    }

    public function user()
    {
        return $this->belongsTo('App\Users\User');
    }

    public function views()
    {
        return $this->hasMany('App\Stats\Views');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments\Comment');
    }

    public function favorites()
    {
        return $this->hasMany('App\Favorites\Favorite');
    }

    public function bump()
    {
        return $this->hasOne('App\Bumps\Bump');
    }

}