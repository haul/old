<?php namespace App\Stats;

use Illuminate\Database\Eloquent\Model;

class Views extends Model {

    protected $table = 'views';

    protected $hidden = ['ip'];

    protected $fillable = ['user_id', 'haul_id', 'ip'];

    public function user()
    {
        $this->hasMany('App\Users\User');
    }

    public function haul()
    {
        $this->hasMany('App\Hauls\Haul');
    }

}