<?php namespace App\Users;

class UserRepository {

    public function getLatestPaginated($limit = 50)
    {
        return User::paginate($limit);
    }

    public function getDeletedPaginated($limit = 5)
    {
        return User::onlyTrashed()->paginate($limit);
    }

    public function findById($id)
    {
        return User::withTrashed()->where('id', $id)->firstOrFail();
    }

    public function findByUsername($username)
    {
        return User::withTrashed()->where('username', $username)->firstOrFail();
    }

    public function getAll()
    {
        return User::all();
    }

    public function save(User $user)
    {
        return $user->save();
    }

    public function search($for)
    {
        $words = explode(' ', $for);

        $users = User::query();

        foreach ($words as $word)
        {
            $users->where('username', 'like', '%' . $word . '%');
        }

        return $users->get();
    }

}