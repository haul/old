<?php namespace App\Users;

use Laracasts\Presenter\Presenter;

use Config;

class UserPresenter extends Presenter {

	public function avatar($size = 'small')
	{
		$filename = $this->entity->avatar ?: 'default.jpg';

		return Config::get('app.url') . '/avatars/' . $size . '/' . $filename;
	}

    public function amazonAvatar($size = 'small')
    {
        $filename = $this->entity->avatar ?: 'default.jpg';

        return 'https://s3-' . getenv('AWS_REGION') . '.amazonaws.com/' . getenv('AWS_BUCKET') . '/theavatars/' . $size . '/' . $filename;
    }

}
