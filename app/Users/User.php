<?php namespace App\Users;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Laracasts\Presenter\PresentableTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, SoftDeletes, PresentableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $presenter = 'App\Users\UserPresenter';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username', 'email', 'password', 'title', 'avatar'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	protected $dates = ['deleted_at'];

	public function isFavorite($haul_id)
	{
		return $this->favorites()->where('haul_id', $haul_id)->where('user_id', $this->id)->first();
	}

	public function hauls()
	{
		return $this->hasMany('App\Hauls\Haul');
	}

	public function comments()
	{
		return $this->hasMany('App\Comments\Comment');
	}

	public function role()
	{
		return $this->hasOne('App\Roles\Role');
	}

	public function views()
	{
		return $this->hasMany('App\Stats\Views');
	}

	public function favorites()
	{
		return $this->hasMany('App\Favorites\Favorite');
	}

}
