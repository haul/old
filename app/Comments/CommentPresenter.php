<?php namespace App\Comments;

use Laracasts\Presenter\Presenter;
use Guzzle\Http\Client;

use App\Users\User;
use App\Hauls\Haul;

class CommentPresenter extends Presenter {

    public function message()
    {
    	$body = $this->body;

    	echo(preg_replace_callback("/\#([1-9][0-9]*+)/", function($id) {
    		if ($haul = Haul::where('id', $id[1])->first())
    		{
    			return '<a href="' . route('haul.show', [$haul->id]) . '"><img src="' . $haul->present()->amazonImageUrl($haul->filename . (($haul->isImage) ? '' : '.jpg'), 'small') . '" data-toggle="tooltip" data-placement="top" class="thumbnail" title="' . $haul->caption . '"></a>';
    		}
    		return '#' . $id[1];
    	}, preg_replace_callback("/\@([A-Za-z0-9_-]\w+)/", function($username) {
    		if ($user = User::where('username', $username[1])->first())
    		{
    			return '<a href="' . route('profile.show', [$user->username]) . '">@' . $user->username . '</a>';
    		}
    		return '@' . $username[1];
    	}, preg_replace_callback('/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', function($link) {
			return '<a href="' . $link[0] . '" target="_blank">' . $link[0] . '</a>';
    	}, $body))));
    }

}
