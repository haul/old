<?php namespace App\Comments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class Comment extends Model {

    use SoftDeletes, PresentableTrait;

    protected $table = 'comments';

    protected $fillable = ['user_id', 'haul_id', 'body'];

    protected $dates = ['deleted_at'];

    protected $presenter = 'App\Comments\CommentPresenter';

    public static function post($user_id, $haul_id, $body)
    {
        $comment = new static(compact('user_id','haul_id', 'body'));

        //$comment->raise(new CommentWasPosted($comment));

        return $comment;
    }

    public function haul()
    {
        return $this->belongsTo('App\Hauls\Haul');
    }

    public function user()
    {
        return $this->belongsTo('App\Users\User');
    }
}