<?php namespace App\Comments;

class CommentRepository {

    public function findById($id)
    {
        return Comment::withTrashed()->where('id', $id)->firstOrFail();
    }

    public function getDeletedPaginated($limit = 5)
    {
        return Comment::onlyTrashed()->latest()->paginate($limit);
    }

    public function findByLatest($limit)
    {
	return Comment::latest()->has('haul')->paginate($limit);
    }

    public function search($for)
    {
        $words = explode(' ', $for);

        $comments = Comment::query();

        foreach ($words as $word)
        {
            $comments->where('body', 'like', '%' . $word . '%');
        }

        return $comments->get();
    }

}
