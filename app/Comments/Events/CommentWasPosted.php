<?php namespace App\Comments\Events;

use App\Comments\Comment;

class CommentWasPosted {

    public $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

}