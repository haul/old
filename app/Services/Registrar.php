<?php namespace App\Services;

use App\Users\User;
use App\Roles\Role;

use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

use Validator;
use Mail;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'username' => 'required|alpha_dash|max:24|unique:users',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:8',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		$user = User::create([
			'username' => $data['username'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);

		Role::create([
			'user_id' => $user->id,
			'level' => 1
		]);
/*
		Mail::send('emails.welcome', ['user' => $user], function($message) use ($user)
		{
		    $message->to($user->email, $user->username)->subject('Welcome to Imagehaul!');
		});
*/
		return $user;
	}

}
