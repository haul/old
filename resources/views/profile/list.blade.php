@extends('layouts.default')

@section('content')
    <h2>Member List</h2>
    <ul class="list-inline">
        @foreach ($members as $member)
        <li>
            <a href="{{ route('profile.show', [$member->username]) }}">
                <img src="{{ $member->present()->amazonAvatar() }}" class="img-circle"><br>{{ $member->username }}
            </a>
        </li>
        @endforeach
    </ul>
@stop
