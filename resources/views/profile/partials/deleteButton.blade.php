@if ($currentUser)
    @if (($currentUser->role->isAdmin() or $currentUser->id == $user->id) and ( ! $user->trashed()))
        <a class="btn btn-sm btn-danger" href="{{ route('profile.delete', [$user->username]) }}">Delete</a>
    @endif
@endif