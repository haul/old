@if ($currentUser)
    @if ($currentUser->role->isAdmin() and $user->trashed())
        <a class="btn btn-sm btn-success" href="{{ route('profile.restore', [$user->username]) }}">Restore</a>
    @endif
@endif