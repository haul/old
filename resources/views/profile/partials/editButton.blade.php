@if ($currentUser)
    @if ($currentUser->role->isAdmin() or $currentUser->id == $user->id)
        <a class="btn btn-sm btn-primary" href="{{ route('profile.edit', [$user->username]) }}">Edit</a>
    @endif
@endif