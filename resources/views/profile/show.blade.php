@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h2>{{ $user->username }} <small><i>{{ $user->title }}</i></small> @include('profile.partials.editButton')</h2>
		<img src="{{ $user->present()->amazonAvatar('large') }}" class="img-thumbnail">
		<br><br>

		<ul class="list-inline">

		@foreach ($user->hauls()->latest()->paginate(25) as $haul)
		    <li>
		        @include('haul.partials.thumb')
		    </li>
		@endforeach

		</ul>
	</div>

	<div class="col-md-6">
		@include ('comment.partials.display', ['comments' => $user->comments()->latest()->has('haul')->paginate(10), 'showThumb' => true])
	</div>
</div>
@stop
