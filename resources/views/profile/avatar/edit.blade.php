@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h1>Change your avatar!</h1>

		@if ($errors->any())
		<div class="alert alert-danger">
			<h3>Uh Oh!</h3>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		{!! Form::model($user, ['route' => ['upload.avatar'], 'files' => true]) !!}

        <div class="form-group">
            <img src="{{ $user->present()->amazonAvatar() }}" class="img-circle">

            {!! Form::label('avatar', 'Avatar:') !!}
            {!! Form::file('avatar', ['class' => 'form-control']) !!}
        </div>

		<div class="form-group">
			<a href="{{ route('profile.show', [$user->username]) }}" class="btn btn-danger">Cancel</a>
			{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
		</div>

		{!! Form::close() !!}
	</div>
</div>
@stop
