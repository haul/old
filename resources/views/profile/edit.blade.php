@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h1>{{$user->username}}</h1>

		@if ($errors->any())
		<div class="alert alert-danger">
			<h3>Uh Oh!</h3>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		{!! Form::model($user, ['route' => ['profile.update', $user->username]]) !!}

		<div class="form-group">
			<img src="{{ $user->present()->amazonAvatar() }}" class="img-circle">

			<a href="{{ route('profile.avatar.edit', [$user->username]) }}" class="btn btn-info">Change Avatar</a>
		</div>

		<div class="form-group">
			@include('profile.partials.deleteButton')
		</div>

		<div class="form-group">
			@include('profile.partials.restoreButton')
		</div>

		<div class="form-group">
			{!! Form::label('title', 'Title:') !!}
			{!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
		</div>

		<div class="form-group">
			<a href="{{ route('profile.show', [$user->username]) }}" class="btn btn-link">Cancel</a>
			{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
		</div>

		{!! Form::close() !!}
	</div>
</div>
@stop
