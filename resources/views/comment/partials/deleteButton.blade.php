@if ($currentUser)
    @if ($currentUser->role->isMod() and ! $comment->trashed())
        <a class="btn btn-sm btn-danger" href="{{ route('comment.delete', [$comment->id]) }}">Delete</a>
    @endif
@endif