@if ($currentUser)
    @if ($currentUser->role->isMod())
        <div class="comment-editbutton"><a href="{{ route('comment.edit', [$comment->id]) }}"><i class="glyphicon glyphicon-cog"></i> Edit</a></div>
    @endif
@endif