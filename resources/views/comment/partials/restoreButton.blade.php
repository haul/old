@if ($currentUser)
    @if ($currentUser->role->isMod() and $comment->trashed())
        <a class="btn btn-sm btn-success" href="{{ route('comment.restore', [$comment->id]) }}">Restore</a>
    @endif
@endif