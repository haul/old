@if ($currentUser)
{!! Form::open(['route' => 'comment.store']) !!}
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-btn">
                {!! Form::submit('Post', ['class' => 'btn']) !!}
            </span>

            {!! Form::text('body', null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>

    {!! Form::hidden('haul', $haul->id) !!}

{!! Form::close() !!}
@endif