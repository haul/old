@foreach ($comments as $comment)
@if ($currentUser)
    @if ($currentUser->id == $comment->user_id)
    	<section class="comment well well-sm">
    @else
		<section class="comment">
    @endif
@else
<section class="comment">
@endif
	<div class="row">
		@if (isset($showThumb) ? $showThumb : false)
	    <div class="col-md-3">
		@if (isset($thumb) ? $thumb : true)
			@include ('haul.partials.thumb', ['haul' => $comment->haul])
		@endif
		</div>
		@endif

		@if (isset($showThumb) ? $showThumb : false)
		<div class="col-md-9">
		@else
		<div class="col-md-12">
		@endif
			@include('comment.partials.editButton')
		    <span>
		        <a href="{{ route('profile.show', [$comment->user->username]) }}">
		            <img src="{{ $comment->user->present()->amazonAvatar() }}" class="img-circle comment-avatar">
		            {{ $comment->user->username }}
		        </a> - <small class="text-muted">{{ $comment->created_at->diffforhumans() }}</small>
		    </span>

		    <p>{{ $comment->present()->message() }}</p>
	    </div>
	</div>
</section>
@endforeach
