@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h1>Edit comment!</h1>

		@include('comment.partials.deleteButton')

		@include('comment.partials.restoreButton')

		<br><br>

        @include('haul.partials.thumb', ['haul' => $comment->haul])

		@if ($errors->any())
		<div class="alert alert-danger">
			<h3>Uh Oh!</h3>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		{!! Form::model($comment, ['route' => ['comment.update', $comment->id]]) !!}

		<div class="form-group">
			{!! Form::label('body', 'Comment:') !!}
			{!! Form::text('body', null, ['class' => 'form-control', 'required' => 'required']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('userid', 'User:') !!}
			<select name="userid">
				@foreach ($members as $member)
				<option value="{{ $member->id }}" {{ ($member->id == $comment->user_id) ? 'selected' : '' }}>{{ $member->username }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<a href="{{ route('haul.show', [$comment->haul->id]) }}" class="btn btn-danger">Cancel</a>
			{!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop