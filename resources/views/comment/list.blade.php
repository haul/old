@extends('layouts.default')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h2>Comments</h2>

        @include ('comment.partials.display', ['comments' => $comments, 'showThumb' => true])

        {!! $comments->render() !!}
    </div>
</div>
@stop