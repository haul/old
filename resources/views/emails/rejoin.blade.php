<h2>Imagehaul has been updated!</h2>
<p>{{ $user->username }} we need you to create a new password so you can login again</p>
<p>Click here to reset your password: {{ url(route('password.remind')) }}</p>