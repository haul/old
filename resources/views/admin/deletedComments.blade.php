@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-12">
        <h1 class="page-header">Deleted comments</h1>

        @if ($comments->count())
        <table class="table table-responsive table-hover table-striped">
            <thead>
                <th>Thumb</th>
                <th>Message</th>
                <th>Owner</th>
                <th>When</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach ($comments as $comment)
                <tr>
                    <td>@include('haul.partials.thumb', ['link' => true, 'haul' => $comment->haul])</td>
                    <td width="50%">{{ $comment->body }}</td>
                    <td>@include('profile.partials.link', ['username' => $comment->user->username])</td>
                    <td>{{ $comment->deleted_at->DiffForHumans() }}</td>
                    <td>
                        @include('comment.partials.editButton')
                        @include('comment.partials.restoreButton')
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $comments->render() !!}
        @else
        <p>No comments deleted.</p>
        @endif
	</div>
</div>
@stop