@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-12">
        <h1 class="page-header">Deleted users</h1>

        @if ($users->count())
        <table class="table table-responsive table-hover table-striped">
            <thead>
                <th>Avatar</th>
                <th>Username</th>
                <th>When</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td><img src="{{ $user->present()->amazonAvatar() }}" class="img-circle"></td>
                    <td>{{ $user->username}}</td>
                    <td>{{ $user->deleted_at->DiffForHumans() }}</td>
                    <td>
                        @include('profile.partials.editButton')
                        @include('profile.partials.restoreButton')
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $users->render() !!}
        @else
        <p>No deleted users.</p>
        @endif
	</div>
</div>
@stop
