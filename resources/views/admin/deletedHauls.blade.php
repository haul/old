@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-12">
        <h1 class="page-header">Deleted hauls</h1>

        @if ($hauls->count())
        <table class="table table-responsive table-hover table-striped">
            <thead>
                <th>Thumb</th>
                <th>Caption</th>
                <th>Owner</th>
                <th>When</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach ($hauls as $haul)
                <tr>
                    <td>@include('haul.partials.thumb', ['link' => false])</td>
                    <td>{{ $haul->caption }}</td>
                    <td>@include('profile.partials.link', ['username' => $haul->user->username])</td>
                    <td>{{ $haul->deleted_at->DiffForHumans() }}</td>
                    <td>
                        @include('haul.partials.editButton')
                        @include('haul.partials.restoreButton')
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $hauls->render() !!}
        @else
        <p>No hauls deleted.</p>
        @endif
	</div>
</div>
@stop