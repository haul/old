@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-12">
        <h1 class="page-header">Haul statistics overview</h1>

        <table class="table table-responsive table-hover table-striped">
            <tbody>
                <tr>
                    <td>No data.</td>
                </tr>
            </tbody>
        </table>
	</div>
</div>
@stop