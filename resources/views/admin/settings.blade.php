@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-3">
        <h1 class="page-header">Settings</h1>

        <a href="#" class="btn btn-warning btn-block disabled" role="button">Clear cache</a>
        <a href="#" class="btn btn-warning btn-block disabled" role="button">Clear temp</a>

        <hr>

        <a href="#" class="btn btn-danger btn-block disabled" role="button">Disable uploads</a>
        <a href="#" class="btn btn-danger btn-block disabled" role="button">Disable registration</a>

        <hr>

        <a href="{{ route('admin.secret') }}" class="btn btn-danger btn-block" role="button">Delete everything</a>

	</div>
</div>
@stop