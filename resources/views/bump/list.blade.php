@extends('layouts.default')

@section('content')
    <ul class="list-inline">
    @foreach ($bumps as $bump)
        <li>
            @include('haul.partials.thumb', ['haul' => $bump->haul, 'size' => 'medium'])
        </li>
    @endforeach
    </ul>
@stop