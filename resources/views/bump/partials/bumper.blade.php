@if ($currentUser)
    @if ( ! $haul->bump)
        {!! Form::open(['route' => 'bump']) !!}
            {!! Form::hidden('haul', $haul->id) !!}
            <button type="submit" class="btn btn-link"><i class="glyphicon glyphicon-hand-up"></i> Bump</button>
        {!! Form::close() !!}
    @endif
@endif