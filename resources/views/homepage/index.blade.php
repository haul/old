@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-8">
	    <ul class="list-inline">
	    @foreach ($latestHauls as $haul)
	        <li>
	            @include('haul.partials.thumb', ['size' => 'medium'])
	        </li>
	    @endforeach
	    </ul>
	<div class="page-header">
		<a class="disabled">Latest Bumps</a>
	</div>

	    <ul class="list-inline">
	    @foreach ($latestBumps as $bump)
			<li>
				@include('haul.partials.thumb', ['haul' => $bump->haul, 'size' => 'medium'])
			</li>
	    @endforeach
	    </ul>
    </div>

    <div class="col-md-4">
<!--    	<div class="page-header">
    		<a href="{{ route('comment.list') }}">Latest Comments</a>
    	</div> -->

    	@include ('comment.partials.display', ['comments' => $latestComments, 'showThumb' => true])
    </div>
</div>

<nav class="navbar">
	<div class="container">
		<div class="col-md-12 text-center">
			<p>All posts on Imagehaul are the responsibility of the individual poster and not the administration of Imagehaul, pursuant to 47 U.S.C. § 230.</p>
			<p>We have not been served any secret court orders and are not under any gag orders.</p>
			<p>To make a DMCA request or report illegal content, please email <a href="mailto:admin@imagehaul.com" target="_blank">admin@imagehaul.com</a>.</p>
		</div>
		<div class="col-md-12 text-center">
			<p>Help us pay for hosting:
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHfwYJKoZIhvcNAQcEoIIHcDCCB2wCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYB9nQ8MjNvtnXY1FAtiWu3abgXD1NabJyMxkqjTicS85r5QqMBkcZAULyBoJwPkDewCVeqqMjkKxiACJTWP82OkNMzUMHG/nnQVKCHLwkrwye7t2qpwKJ5Sr8/2fnbxmOXBiioppRcDvsceqrW747LecPZH4lWk65RF/YbP5W3MqTELMAkGBSsOAwIaBQAwgfwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIo9PhnHavs1eAgdhhl65rgMZpfG2s87fTmNVKA/Uz1Tsn62A0ZyFKqAFInNgLjynbZFZywNMVjWupEaoSRCh0PlrovlNU2cv0lYisbWQHEudMpq0rvtnj392/uzP/Ra1c5hgwFTfmluXPPgfHxHCDo9EowxLs/p8+q27kYxSZS851KF1ngoHtaNmqs0zwFEyRaQoQyqm5lbyyedtZFzeeq+EoRZOu6OyeqY7N4xZFicAIK9fsHn5X82exQi41QAcvuX1X/72xo5tjOebBzRMzELJfPoWS/OxKY4iEs/JmEfx4egqgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xNTA1MzExOTIyNTVaMCMGCSqGSIb3DQEJBDEWBBQT+ax2+dZptl0xwpMsZ5Bn4pJNgDANBgkqhkiG9w0BAQEFAASBgDTq15Nd2cm2n2ZqcXulhg9OfpQrfT4cFG/1DOCZS5GZt7mypHqM7jvOXg4VniufO+XvnGIh+xj+O1pZqMCdWlBTrRuIqzf6O4k877SHsySYwvcelNHEst4TNeQoUBC5HQVzpARTIXT3Sfx4MmL1fE44/r0SMfvgYUHs7goGBrDE-----END PKCS7-----">
				<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" style="border:0;" alt="PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
			</p>
		</div>
	</div>
</nav>
@stop
