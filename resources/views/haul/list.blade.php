@extends('layouts.default')

@section('content')
    <ul class="list-inline">
    @foreach ($hauls as $haul)
        <li>
            @include('haul.partials.thumb', ['size' => 'medium'])
        </li>
    @endforeach
    </ul>

    {!! $hauls->render() !!}
@stop