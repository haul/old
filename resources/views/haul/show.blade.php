@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-8">
        <h2 class="text-center">{{ $haul->caption }}</h2>

        @include('haul.partials.pagination')

        @include('haul.partials.image', ['link' => false])
    </div>

    <div class="col-md-4">
        <h2><small><a href="{{ route('profile.show', [$haul->user->username]) }}"><img src="{{ $haul->user->present()->amazonAvatar() }}" class="img-circle"> {{ $haul->user->username }}</a> @if ($haul->user->title)- <i>{{ $haul->user->title }}</i>@endif</small></h2>
        <p class="text-muted">Posted {{ $haul->created_at->diffforhumans() }}</p>

        <hr>

        <ul class="nav nav-tabs">
            <li class="active"><a class="disabled"><i class="glyphicon glyphicon-comment"></i> <span class="label label-primary">{{ $haul->comments->count() }}</span></a></li>
            <li>@include('bump.partials.bumper')</li>
            <li>@include('favorite.partials.star')</li>
            <li class="navbar-right">@include('haul.partials.editButton')</li>
        </ul>

        <br>

        @include('comment.partials.form')

        @include('comment.partials.display', ['comments' => $haul->comments->reverse()])
    </div>
</div>
@stop
