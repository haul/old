@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h1>Delete!</h1>

		@include('haul.partials.thumb')

		@if ($errors->any())
		<div class="alert alert-danger">
			<h3>Uh Oh!</h3>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		{!! Form::model($haul, ['method' => 'delete', 'route' => ['haul.delete', $haul->id]]) !!}

		<div class="form-group">
			{!! Form::label('reason', 'Reason:') !!}
			{!! Form::text('reason', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			<a href="{{ route('haul.edit', [$haul->id]) }}" class="btn btn-danger">Cancel</a>
			{!! Form::submit('Delete', ['class' => 'btn btn-success']) !!}
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop