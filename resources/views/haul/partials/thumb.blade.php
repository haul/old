@if (isset($link) ? $link : true)
<a href="{{ route('haul.show', [$haul->id]) }}">
@endif
    <img src="{{ $haul->present()->amazonImageUrl($haul->filename . ($haul->isImage ? '' : '.jpg'), (isset($size) ? $size : 'small')) }}"
	@if (isset($caption) ? $caption : true)
	class="thumbnail" data-toggle="tooltip" data-placement="top" title="{{ $haul->caption }}"
	@endif
 />
@if (isset($link) ? $link : true)
</a>
@endif
