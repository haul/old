@if ($currentUser)
    @if ($currentUser->role->isMod() or $currentUser->id == $haul->user_id)
        <a class="btn" href="{{ route('haul.edit', [$haul->id]) }}"><i class="glyphicon glyphicon-cog"></i> Edit</a>
    @endif
@endif