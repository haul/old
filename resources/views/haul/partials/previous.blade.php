@if ($haul->getPrevious())
<li><a href="{{ route('haul.show', [$haul->getPrevious()]) }}">Older <i class="glyphicon glyphicon-arrow-right"></i></a></li>
@else
<li class="disabled"><span>Older <i class="glyphicon glyphicon-minus-sign"></i></span></li>
@endif