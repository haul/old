@if (isset($link) ? $link : true)
<a href="{{ route('haul.show', [$haul->id]) }}">
@endif
@if ($haul->isImage)
    <img src="{{ $haul->present()->amazonImageUrl($haul->filename) }}" class="img-responsive img-thumbnail haul" style="display: block" />
@else
	<video src="{{ $haul->present()->amazonImageUrl($haul->filename) }}" autoplay preload loop controls class="img-responsive img-thumbnail haul" style="display: block"></video>
@endif
@if (isset($link) ? $link : true)
</a>
@endif
