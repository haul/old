@if ($currentUser)
    @if (($currentUser->role->isMod() or $currentUser->id == $haul->user_id) and ($haul->trashed()))
        <a class="btn btn-sm btn-success" href="{{ route('haul.restore', [$haul->id]) }}">Restore</a>
    @endif
@endif