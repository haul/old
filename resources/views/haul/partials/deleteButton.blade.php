@if ($currentUser)
    @if (($currentUser->role->isMod() or $currentUser->id == $haul->user_id) and ( ! $haul->trashed()))
        <a class="btn btn-sm btn-primary" href="{{ route('haul.delete', [$haul->id]) }}">Delete</a>
    @endif
@endif