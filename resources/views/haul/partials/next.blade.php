@if ($haul->getNext())
<li><a href="{{ route('haul.show', [$haul->getNext()]) }}"><i class="glyphicon glyphicon-arrow-left"></i> Newer</a></li>
@else
<li class="disabled"><span><i class="glyphicon glyphicon-minus-sign"></i> Newer</span></li>
@endif