<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			@if (isset($title))
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modalLabel">{{ $title or '' }}</h4>
			</div>
			@endif

			@if (isset($body))
			<div class="modal-body">
				{!! $body or '' !!}
			</div>
			@endif

			@if (isset($footer))
			<div class="modal-footer">
				{!! $footer or '' !!}
			</div>
			@endif
		</div>
	</div>
</div>