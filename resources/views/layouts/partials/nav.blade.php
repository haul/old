<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ route('home') }}"><span class="logo"></span> Imagehaul</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="{{ route('haul.random') }}"><i class="glyphicon glyphicon-random"></i> Random</a></li>
				<li><a href="{{ route('upload.form') }}"><i class="glyphicon glyphicon-cloud-upload"></i> Up</a></li>
			</ul>
			{!! Form::open(['route' => 'search', 'method' => 'get', 'class' => 'navbar-form navbar-left']) !!}
			<div class="form-group hidden-sm">
				<input type="text" name="for" class="form-control" placeholder="Search">
			</div>
			{!! Form::close() !!}
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> Nav <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li class="visible-sm-block"><a href="{{ route('search') }}"><i class="glyphicon glyphicon-search"></i> Search</a></li>
						<li><a href="{{ route('haul.list') }}"><i class="glyphicon glyphicon-th"></i> Hauls</a></li>
						<li><a href="{{ route('comment.list') }}"><i class="glyphicon glyphicon-comment"></i> Comments</a></li>
						<li><a href="{{ route('profile.list') }}"><i class="glyphicon glyphicon-globe"></i> Members</a></li>
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
			@if ($currentUser)
				@if ($currentUser->role->isMod())
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-flash"></i> Admin <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ route('admin.reports.overview') }}"><i class="glyphicon glyphicon-list-alt"></i> Statistics</a></li>
						<li class="divider"></li>
						<li><a href="{{ route('admin.deletedHauls') }}"><i class="glyphicon glyphicon-th-list"></i> Hauls</a></li>
						<li><a href="{{ route('admin.deletedComments') }}"><i class="glyphicon glyphicon-comment"></i> Comments</a></li>
						<li><a href="{{ route('admin.deletedUsers') }}"><i class="glyphicon glyphicon-user"></i> Users</a></li>
						<div class="divider"></div>
						<li><a href="{{ route('admin.settings') }}"><i class="glyphicon glyphicon-wrench"></i> Settings</a></li>
					</ul>
				</li>
				@endif
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ $currentUser->present()->amazonAvatar() }}" class="avatar img-circle">
						{{ $currentUser->username }} <span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ route('profile.show', [$currentUser->username]) }}"><i class="glyphicon glyphicon-eye-open"></i> Profile</a></li>
						<li><a href="{{ route('profile.edit', [$currentUser->username]) }}"><i class="glyphicon glyphicon-cog"></i> Settings</a></li>
						<li class="divider"></li>
						<li><a href="{{ route('auth.logout') }}"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
					</ul>
				</li>
			@else
				<li><a href="#" data-toggle="modal" data-target="#loginModal" class="hidden-xs visible-sm-block visible-md-block visible-lg-block visible-xl-block"><i class="glyphicon glyphicon-record"></i> Login</a></li>
				<li><a href="{{ route('auth.login') }}" class="visible-xs hidden-sm hidden-md hidden-lg hidden-xl"><i class="glyphicon glyphicon-record"></i> Login</a></li>
				<!-- <li><a href="{{ route('auth.register') }}">Sign Up</a></li> -->
			@endif
			</ul>
		</div>
	</div>
</nav>
