<div id="aprilfools2015">
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april1.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april2.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april3.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april4.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april5.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april6.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april7.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april8.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april9.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april10.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april11.jpg" class="aprilfools"></a>
	<a href="{{ route('haul.random') }}"><img src="http://imagehaul.com/assets/img/aprilfools/2015/april12.jpg" class="aprilfools"></a>
</div>

<script>
	$(function () {
	    var minSpeed = .02;
	    var maxSpeed = .06;
	    var varSpeed = .005;

	    function startBounce(elements) {
	        elements.each(function(i, element) {
	            var el = $(element);
	            var width = window.innerWidth - el.outerWidth();
	            var height = window.innerHeight - el.outerHeight();

	            var vertSpeed = ((Math.random() * (maxSpeed - minSpeed)) + minSpeed);
	            var horzSpeed = ((Math.random() * (maxSpeed - minSpeed)) + minSpeed);

	            bounce(el, vertSpeed, height, 'top');
	            bounce(el, horzSpeed, width, 'left');
	        });
	    }

	    function bounce(img, speed, max, dir) {
	        speed += ((Math.random() * varSpeed) - (varSpeed / 2));
	        speed = speed < minSpeed ? minSpeed : (speed > maxSpeed ? maxSpeed : speed)

	        var time = max / speed;
	        var position = img.position();
	        var target = (position[dir] < 2) ? max : 0;
	        var style = {
	            queue: false
	        };

	        style[dir] = target;

	        img.animate(style, {
	            duration: time,
	            queue: false,
	            easing: "linear",
	            complete: function() {
	                bounce(img, time, max, dir);
	            }
	        });
	    }

	    var prank = $('#aprilfools2015');

	    prank.children().each(function(i, v) {
    		$(v).css('position', 'absolute');
	    	startBounce($(v));
	    });


	});
</script>