<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="If it's not haul'd, it's not funny.">
		<meta name="author" content="tr3nx">

		<title>
				@if (Request::server('HTTP_HOST') == "imageheil.com")
				卍 Imageheil 卍
				@else
				Imagehaul
				@endif
		</title>

		{!! HTML::style('assets/css/vendor/flaty.min.css') !!}
		{!! HTML::style('assets/css/style.css') !!}
		@if (Request::server('HTTP_HOST') == "imageheil.com")
		{!! HTML::style('assets/css/aprilfools/heil.css') !!}
		@endif

		<link rel="apple-touch-icon" sizes="57x57" href="https://imagehaul.com/assets/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="https://imagehaul.com/assets/favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="https://imagehaul.com/assets/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="https://imagehaul.com/assets/favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="https://imagehaul.com/assets/favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="https://imagehaul.com/assets/favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="https://imagehaul.com/assets/favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="https://imagehaul.com/assets/favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="https://imagehaul.com/assets/favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="https://imagehaul.com/assets/favicons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="https://imagehaul.com/assets/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="https://imagehaul.com/assets/favicons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="https://imagehaul.com/assets/favicons/favicon-16x16.png">
		<link rel="manifest" href="https://imagehaul.com/assets/favicons/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="https://imagehaul.com/assets/favicons/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!--[if lt IE 9]>
				{!! HTML::script('assets/js/vendor/html5shiv.js') !!}
				{!! HTML::script('assets/js/vendor/respond.min.js') !!}
		<![endif]-->

	</head>
	<body>

	@include('layouts.partials.nav')

	<div class="container" id="page">
		@include('flash::message')

		@yield('content')
	</div>

	@include('layouts.partials.popups.login')
	@include('layouts.partials.footer')


	{!! HTML::script('assets/js/vendor/jquery-2.1.1.min.js') !!}
	{!! HTML::script('assets/js/vendor/bootstrap.min.js') !!}

	<script>
		$(function () {
			$('[data-toggle="tooltip"]').tooltip();

			$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

			$('.modal').on('shown.bs.modal', function() {
				$(this).find('[data-ih-autofocus]').focus();
			});

			var haul = $('video.haul').each(function() {
				this.volume = 0.6;
			});
		});
	</script>

	</body>
</html>
