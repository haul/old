@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h1>Upload!</h1>

		@if ($errors->any())
		<div class="alert alert-danger">
			<h3>Uh Oh!</h3>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		{!! Form::open(['route' => ['upload.haul'], 'files' => true]) !!}

		<div class="form-group">
		    {!! Form::label('haul', 'Pick a haul:') !!}
			{!! Form::file('haul', ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
            {!! Form::label('caption', 'Caption:') !!}
            {!! Form::text('caption', null, ['class' => 'form-control', 'placeholder' => 'Caption for the haul!']) !!}
        </div>

        <div class="form-group">
            {!! Form::checkbox('tos', 0, ['class' => 'form-control']) !!}
            <label for="tos">I accept these <a href="{{ route('pages.tos') }}">terms</a>.</label>
        </div>

		<div class="form-group">
			{!! Form::submit('Upload', ['class' => 'btn btn-success']) !!}
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop