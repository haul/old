@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-12">
		<form class="form-inline">
			<div class="form-group">
				<input type="text" name="for" class="form-control" placeholder="Search" @if(isset($for))value="{{ $for }}"@endif>
				<input type="submit" class="btn btn-primary" value="Search">
			</div>
		</form>

		@if(isset($for))
		<h1>Searching "{{ $for }}"!</h1>

		<ul class="list-inline">
			@foreach ($hauls as $haul)
			<li>
				@include('haul.partials.thumb')
			</li>
			@endforeach
		</ul>
		@endif

	</div>
</div>
@stop