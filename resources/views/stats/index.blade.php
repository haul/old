@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-md-12 text-center">
		<h1>We did it!</h1>
		<h1 style="font-size: 70px">15,000 Hauls!</h1>
		<h3>Special thanks to everyone who has ever used this website and continues to use it!  This is a community driven website and we could never have made it this far without everyone (even the bots)!</h3>
		<h3>Let's see some interesting stats over the last several months since our last milestone of 14k and before.</h3>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<h3>The big 15k</h3>
		<table class="table table-striped">
			<tr>
				<td>@include('haul.partials.thumb', ['haul' => $data['fifteenthousandthhaul'], 'size' => 'large'])</td>
				<td><h2><small><a href="{{ route('profile.show', [$data['fifteenthousandthhaul']->user->username]) }}"><img src="{{ $data['fifteenthousandthhaul']->user->present()->amazonAvatar() }}" class="img-circle"> {{ $data['fifteenthousandthhaul']->user->username }}</a> @if ($data['fifteenthousandthhaul']->user->title)- <i>{{ $data['fifteenthousandthhaul']->user->title }}</i>@endif</small></h2><p class="text-muted">Posted {{ $data['fifteenthousandthhaul']->created_at->diffforhumans() }}</p></td>
			</tr>
			<tr>
				<td>The 14k Milestone was reached</td>
				<td>{{ $data['sinceLastMilestone'] }}</td>
			</tr>
			<tr>
				<td>Total filesize uploaded since last milestone</td>
				<td>{{ $data['filesizetotal'] }}</td>
			</tr>
		</table>

		<h3>Upload totals since 14k</h3>
		<table class="table table-striped">
			<tr>
				<td>{{ $data['webmtotal'] }} Webm</td>
			</tr>
			<tr>
				<td>{{ $data['giftotal'] }} Gif</td>
			</tr>
			<tr>
				<td>{{ $data['pngtotal'] }} Png</td>
			</tr>
			<tr>
				<td>{{ $data['jpgtotal'] }} Jpeg</td>
			</tr>
		</table>

		<h3>Top Uploaders since 14k</h3>
		<table class="table table-striped">
			@foreach ($data['topuploaders'] as $uploaders)
			<tr>
				<td><a href="{{ route('profile.show', [$uploaders['user']->username]) }}"><img src="{{ $uploaders['user']->present()->amazonAvatar() }}" class="img-circle"></a> <a href="{{ route('profile.show', [$uploaders['user']->username]) }}">{{ $uploaders['user']->username }}</a></td>
				<td><h4>{{ $uploaders['uploadcount'] }}</h4></td>
			</tr>
			@endforeach
		</table>

		<h3>Comment Stats</h3>
		<table class="table table-striped">
			<tr>
				<td>Comments since last milestone</td>
				<td>{{ $data['commenttotal'] }}</td>
			</tr>
		</table>

		<h3>Favorite Stats</h3>
		<table class="table table-striped">
			<tr>
				<td>Favorites since last milestone</td>
				<td>{{ $data['favoritetotal'] }}</td>
			</tr>
		</table>

		<h3>Random website stats</h3>
		<table class="table table-striped">
			<tr>
				<td>Total webm count</td>
				<td>{{ $data['webmtotalcount'] }}</td>
			</tr>
			<tr>
				<td>Total registered users</td>
				<td>{{ $data['totalregistered'] }}</td>
			</tr>
			<tr>
				<td>Combined width dimension</td>
				<td>{{ $data['pixelwidth'] }} px</td>
			</tr>
			<tr>
				<td>Combined height dimension</td>
				<td>{{ $data['pixelheight'] }} px</td>
			</tr>
			<tr>
				<td>Combined dimensions</td>
				<td>{{ $data['pixelwidth'] + $data['pixelheight'] }} px</td>
			</tr>
			<tr>
				<td>Total upload count</td>
				<td>{{ $data['totaluploadcount'] }}</td>
			</tr>
			<tr>
				<td>Total upload filesize</td>
				<td>{{ $data['totaluploadfilesize'] }}</td>
			</tr>
			<tr>
				<td>Daily average of page views</td>
				<td>~{{ $data['dailyviewcount'] }}</td>
			</tr>
			<tr>
				<td>Imagehaul Version number</td>
				<td>{{ $data['imagehaulversion'] }}</td>
			</tr>
			<tr>
				<td>Next version of imagehaul</td>
				<td>{{ $data['nextversion'] }}</td>
			</tr>
			<tr>
				<td>Time it took to make this page</td>
				<td>~{{ $data['pagecreationtime'] }} hours</td>
			</tr>
		</table>

		<h3>Thanks again to everyone, I'll see you at the next milestone, 16000!</h3>
    </div>
</div>
<br><br>

<nav class="navbar">
	<div class="container">
		<div class="col-md-12 text-center">
			<p>All posts on Imagehaul are the responsibility of the individual poster and not the administration of Imagehaul, pursuant to 47 U.S.C. § 230.</p>
			<p>We have not been served any secret court orders and are not under any gag orders.</p>
			<p>To make a DMCA request or report illegal content, please email <a href="mailto:admin@imagehaul.com" target="_blank">admin@imagehaul.com</a>.</p>
		</div>
		<div class="col-md-12 text-center">
			<p>Help us pay for hosting:
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHfwYJKoZIhvcNAQcEoIIHcDCCB2wCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYB9nQ8MjNvtnXY1FAtiWu3abgXD1NabJyMxkqjTicS85r5QqMBkcZAULyBoJwPkDewCVeqqMjkKxiACJTWP82OkNMzUMHG/nnQVKCHLwkrwye7t2qpwKJ5Sr8/2fnbxmOXBiioppRcDvsceqrW747LecPZH4lWk65RF/YbP5W3MqTELMAkGBSsOAwIaBQAwgfwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIo9PhnHavs1eAgdhhl65rgMZpfG2s87fTmNVKA/Uz1Tsn62A0ZyFKqAFInNgLjynbZFZywNMVjWupEaoSRCh0PlrovlNU2cv0lYisbWQHEudMpq0rvtnj392/uzP/Ra1c5hgwFTfmluXPPgfHxHCDo9EowxLs/p8+q27kYxSZS851KF1ngoHtaNmqs0zwFEyRaQoQyqm5lbyyedtZFzeeq+EoRZOu6OyeqY7N4xZFicAIK9fsHn5X82exQi41QAcvuX1X/72xo5tjOebBzRMzELJfPoWS/OxKY4iEs/JmEfx4egqgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xNTA1MzExOTIyNTVaMCMGCSqGSIb3DQEJBDEWBBQT+ax2+dZptl0xwpMsZ5Bn4pJNgDANBgkqhkiG9w0BAQEFAASBgDTq15Nd2cm2n2ZqcXulhg9OfpQrfT4cFG/1DOCZS5GZt7mypHqM7jvOXg4VniufO+XvnGIh+xj+O1pZqMCdWlBTrRuIqzf6O4k877SHsySYwvcelNHEst4TNeQoUBC5HQVzpARTIXT3Sfx4MmL1fE44/r0SMfvgYUHs7goGBrDE-----END PKCS7-----">
				<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" style="border:0;" alt="PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
			</p>
		</div>
	</div>
</nav>
@stop
