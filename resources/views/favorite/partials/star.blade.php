@if ($currentUser)
    @if ( ! $currentUser->isFavorite($haul->id))
        {!! Form::open(['route' => 'favorite']) !!}
            {!! Form::hidden('haul', $haul->id) !!}
            <button type="submit" class="btn btn-link"><i class="glyphicon glyphicon-heart-empty"></i> Favorite <span class="label label-success">{{ $haul->favorites->count() }}</span></button>
        {!! Form::close() !!}
    @else
        {!! Form::open(['route' => 'favorite.delete']) !!}
            {!! Form::hidden('haul', $haul->id) !!}
            <button type="submit" class="btn btn-link"><i class="glyphicon glyphicon-heart"></i> Unfavorite <span class="label label-success">{{ $haul->favorites->count() }}</span></button>
        {!! Form::close() !!}
    @endif
@endif