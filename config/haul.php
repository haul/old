<?php

return [
	'haulThumbSizes' => [
		'large' => 200,
		'medium' => 100,
		'small' => 50
	],

	'avatarThumbSizes' => [
		'large' => 200,
		'medium' => 100,
		'small' => 50
	],

	'thumbPath' => storage_path() . '/tmp/',

	'formNames' => [
		'hauls' => 'haul',
		'avatars' => 'avatar'
	],

	'randoms' => 100,

	'bumpcount' => 18,

];