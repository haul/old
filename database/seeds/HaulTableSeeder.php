<?php

use App\Hauls\Haul;
use Illuminate\Contracts\Filesystem\Cloud;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class HaulTableSeeder extends Seeder {

    protected $cloud;

    function __construct(Cloud $cloud)
    {
        $this->cloud = $cloud;
    }

    public function run()
    {
        $faker = Faker::create();

        $files = $this->cloud->files('hauls');

        foreach ($files as $file)
        {
            Haul::create([
                'user_id' => rand(1,12),
                'filename' => str_replace('hauls/', '', $file),
                'hash' => $faker->sha1,
                'ip' => ip2long('127.0.0.1'),
                'caption' => $faker->word,
                'size' => 1234,
                'ext' => 'jpg',
                'width' => 600,
                'height' => 200
            ]);
        }
    }
}