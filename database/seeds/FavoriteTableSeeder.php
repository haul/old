<?php

use App\Favorites\Favorite;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FavoriteTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 12) as $index)
        {
            Favorite::create([
                'user_id' => $index,
                'haul_id' => rand(1, 8)
            ]);
        }
    }

}