<?php

use App\Users\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker::create();

        User::create([
            'username' => 'tr3nx',
            'email' => 'me@tr3nx.net',
            'title' => $faker->word,
            'password' => 'pass',
            'avatar' => 'a47df168cc3e5e905e5a4c4469c1afb8.jpg'
        ]);

        User::create([
            'username' => 'chadd',
            'email' => 'chadd@tr3nx.net',
            'title' => $faker->word,
            'password' => 'pass'
        ]);

        foreach (range(1, 15) as $index)
        {
            User::create([
                'username' => $faker->firstname,
                'email' => $faker->email,
                'title' => $faker->word,
                'password' => 'pass'
            ]);
        }
    }

}
