<?php

use App\Comments\Comment;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CommentTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 25) as $index)
        {
            Comment::create([
                'user_id' => rand(1, 10),
                'haul_id' => rand(1, 17),
                'body' => $faker->paragraph()
            ]);
        }
    }

}