<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ViewsTableSeeder extends Seeder {

    public function run()
    {
        $data = [];

        foreach (range(1, 3) as $index)
        {
            $data[] = [
                'haul_id' => $index,
                'user_id' => 2,
                'ip' => ip2long('127.0.0.1'),
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()')
            ];

            $data[] = [
                'haul_id' => $index,
                'user_id' => 1,
                'ip' => ip2long('127.0.0.1'),
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()')
            ];
        }

        DB::table('views')->insert($data);

    }

}