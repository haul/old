<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('HaulTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('CommentTableSeeder');
		$this->call('RoleTableSeeder');
		$this->call('FavoriteTableSeeder');
		//$this->call('ViewsTableSeeder');
	}

}
