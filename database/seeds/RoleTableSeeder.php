<?php

use App\Roles\Role;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RoleTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 19) as $index)
        {
            Role::create([
                'user_id' => $index,
                'level' => 1
            ]);
        }

        Role::where('user_id', 1)->update(['level' => 5]);
    }

}